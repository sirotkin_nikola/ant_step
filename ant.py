
class Ant_step:
    def __init__(self, start_x, start_y):
        self.start_x = start_x
        self.start_y = start_y

    @staticmethod
    def check_cell(x, y):
        function = lambda n: sum(int(digit) for digit in str(n))
        return function(x) + function(y) <= 25

    def count_accessible_cells(self):
        queue = [(self.start_x, self.start_y)]
        visited = set()

        while queue:
            x, y = queue.pop(0)
            if (x, y) in visited or not self.check_cell(x, y):
                continue

            visited.add((x, y))

            for dx, dy in [(0, 1), (0, -1), (-1, 0), (1, 0)]:
                new_x, new_y = x + dx, y + dy
                if self.check_cell(new_x, new_y):
                    queue.append((new_x, new_y))

        return len(visited)


inst = Ant_step(1000, 1000)
print(inst.count_accessible_cells())

